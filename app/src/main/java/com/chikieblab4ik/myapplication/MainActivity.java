package com.chikieblab4ik.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "anton";
    private ListView listView;

    Bankomat b1 = new Bankomat("город самарский улица чечни 44, кв 5",  "Банкомат", "09:00-20:00");
    Bankomat b2 = new Bankomat("Баныкина 101010101 ауе",  "Отделение", "11:00-22:00");
    Bankomat b3 = new Bankomat("Макарычев миксер пупсень 13",  "Отделение", "08:00-18:00");
    Bankomat b4 = new Bankomat("Адрес мне лень придумывать((( 74",  "Отделение", "09:00-21:00");
    ArrayList<Bankomat> bankomatArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        listView = findViewById(R.id.listDepartaments);
        getSupportActionBar().hide();
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications, R.id.navigation_help)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        bankomatArray.add(b1);
        bankomatArray.add(b2);
        bankomatArray.add(b3);
        bankomatArray.add(b4);

        ArrayList<Departament> departamentsArr = makeDepartamentArray();
        AdapterDepartaments adapter = new AdapterDepartaments(this, departamentsArr); // Он не пустой
        listView.setAdapter(adapter);
    }

    public Departament makeOneDepartament(Bankomat tempBank) {
        String adressD = tempBank.getAdress();
        String typeD = tempBank.getType();
        String timeD = tempBank.getTimeWork();
        Departament departament = new Departament(adressD, typeD, timeD);
        return departament;
    }

    public ArrayList<Departament> makeDepartamentArray() {
        ArrayList<Departament> departamentsArrayList = new ArrayList<>();
        for (int i = 0; i < bankomatArray.size(); i++) {
            Departament departament = makeOneDepartament(bankomatArray.get(i));
            Log.i(TAG, departament + "");
            departamentsArrayList.add(departament);
        }
        return  departamentsArrayList;
    }

}

class Bankomat {

    public String adress;
    public String type;
    public String timeWork;

    public Bankomat(String adress, String type, String timeWork) {
        this.adress = adress;
        this.type = type;
        this.timeWork = timeWork;
    }

    public String getAdress() {
        return adress;
    }

    public String getType() {
        return type;
    }

    public String getTimeWork() {
        return timeWork;
    }
}