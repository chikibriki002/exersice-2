package com.chikieblab4ik.myapplication;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Departament {

    private static final String TAG = "papa";
    private String adress;
    private String type;
    private String time;
    private boolean statusWork;

    public Departament(String adress, String type, String time) {
        this.adress = adress;
        this.type = type;
        this.time = time;
        statusWork = getStatusWork();
    }

    public int timeToInt(String time) {
        return Integer.parseInt(time.substring(0, 2)) * 60 + Integer.parseInt(time.substring(3, 5));
    }

    public boolean getStatusWork() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date timeNow = new Date();
        String morkovka = dateFormat.format(timeNow);

        String timeStartWork = time.substring(0, 5); // Время открытия
        String timeEndWork = time.substring(6, 11); // Время закрытия
        int minutesStart = timeToInt(timeStartWork);
        int minutesEnd = timeToInt(timeEndWork);
        int minutesNow = timeToInt(morkovka);
        if (minutesNow >= minutesStart & minutesNow < minutesEnd) {
            Log.i(TAG, "Работает");
            return true;
        } else if (minutesNow < minutesStart || minutesNow > minutesEnd){
            Log.i(TAG, "Закрыто");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "Departament{" +
                "adress='" + adress + '\'' +
                ", type='" + type + '\'' +
                ", time='" + time + '\'' +
                ", statusWork=" + statusWork +
                '}';
    }

    public String getAdress() {
        return adress;
    }

    public String getType() {
        return type;
    }

    public String getTime() {
        return time;
    }

    public boolean isStatusWork() {
        return statusWork;
    }
}
