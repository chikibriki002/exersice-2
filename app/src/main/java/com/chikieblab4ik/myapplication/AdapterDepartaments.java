package com.chikieblab4ik.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterDepartaments extends BaseAdapter {

    private static final String TAG = "papa";
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Departament> departamentArray;

    AdapterDepartaments(Context context, ArrayList<Departament> departamentArrayList) {
        ctx = context;
        departamentArray = departamentArrayList;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.i(TAG, "AdapterDepartaments: 1" );
    }

    @Override
    public int getCount() {
        return departamentArray.size();
    }

    @Override
    public Object getItem(int position) {
        return departamentArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View viewC, ViewGroup viewGroup) {
        try {
            View view = viewC;
            Log.i(TAG, "getView: 123");
            if (view == null) {
                Log.i(TAG, "getView: 1234");
                view = lInflater.inflate(R.layout.item, viewGroup, false);
                Log.i(TAG, "getView: 12345");
            }
            Log.i(TAG, "1");
            Departament oneDepartament = departamentArray.get(position);
            Log.i(TAG, "2");
            ((TextView) (view).findViewById(R.id.tvAdress)).setText(oneDepartament.getAdress());
            Log.i(TAG, "3");
            ((TextView) (view).findViewById(R.id.tvType)).setText(oneDepartament.getType());
            Log.i(TAG, "4");
            ((TextView) (view).findViewById(R.id.tvTime)).setText(oneDepartament.getTime());
            Log.i(TAG, "5");
            if (oneDepartament.isStatusWork()) {
                ((TextView) (view).findViewById(R.id.tvStatus)).setText("Работает");
                ((TextView) (view).findViewById(R.id.tvStatus)).setTextColor(Color.parseColor("#00FF00"));
                Log.i(TAG, "Рa");
            } else {
                ((TextView) (view).findViewById(R.id.tvStatus)).setText("Закрыто");
                ((TextView) (view).findViewById(R.id.tvStatus)).setTextColor(Color.parseColor("#FF0000"));
            }
            Log.i(TAG, "Зa");


            return view;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getView: " + e);
            return null;
        }
    }
}
